#! /usr/bin/env python3


import sys


def line(number: int):
    linea = str(number) * number
    return linea

def triangle(number: int):
    triangulo = ""
    if number > 9:
        raise ValueError('El parámetro debe ser menor o igual a 9')
    else:
         for i in range(1, number + 1):
             triangulo = triangulo + line(i) + "\n"
         return triangulo


def main():
    number: int = sys.argv[1]
    text = triangle(int(number))
    print(text)


if __name__ == '__main__':
    main()